import copy
import sys; sys.path.append('../')
import datetime
from datetime import timedelta
from collections import defaultdict
import cPickle as pkl
import core_ml
import helpers
import numpy as np

MATCH_LEN=7*12
N_PROCESS=100
MAX_ITERS=100
N_ALIGN=5
EMR_RESULTS_FNAME='emr_results'
TABLE_NAME='kc_test'

def revert_date(time_from_year_0, year_0='1900-01-01'):
    date_format = "%Y-%m-%d"
    timeobj=datetime.datetime.strptime(year_0, date_format)+timedelta(days=int(time_from_year_0*30.4))
    return timeobj.strftime(date_format)

oil_ds = pkl.load(open('data_oil.pkl', 'rb'))
[dset.preprocess(standardize=False) for dset in oil_ds]

# Load data from the EMR lines file
alignments_per_lambda=defaultdict(dict)
with open(EMR_RESULTS_FNAME, 'rb') as f_res:
    for line_id, line in enumerate(f_res.readlines()):
        sys.stdout.write('\r')
        sys.stdout.write('Loading line {}'.format(line_id))
        sys.stdout.flush()
        
        try:
            split_line = line.decode('string_escape').split('\t')
        except:
            continue
    
        lbd, dset_id = pkl.loads(split_line[0])
        lbd = float(lbd)
        dset_id = int(dset_id)
        (align_ids, align_pts, align_starts) = pkl.loads(split_line[1])
        alignments_per_lambda[lbd][dset_id] = (align_ids, align_pts, align_starts)                                   

def compute_well_prediction(dset_id, smoothness=20, n_align=5, db_data=defaultdict(list)):
    print('\n\nComputing prediction for well {}...'.format(well_id))
    best_err = np.inf
    best_W = 0
    best_lbd = 0
    best_model = None
    for lbd in [0.05]: #alignments_per_lambda.keys():
        # Load the EMR results.
        (align_ids, align_starts, align_pts) = alignments_per_lambda[lbd][dset_id]

        # Gather aligned datasets data.
        n_align=min(n_align, len(align_ids))
        target_dset = oil_ds[dset_id]
        auxs = [core_ml.align_sequences(target_dset, oil_ds[align_ids[aux_id]], align_pts[aux_id],
                                        align_starts[aux_id], MATCH_LEN) for aux_id in range(n_align)]
        
        # Compute the prediction.
        (model, win_W, valid_err) = core_ml.multi_gp([target_dset]+auxs, max_iters=MAX_ITERS,
                              fixed_hypers={'W':32., 'l':smoothness, 'noise':None, 'kappa':None},# valid_period=None, n_cv_vals=None)
                                                     valid_period=36, n_cv_vals=10)
        
        if valid_err < best_err:
            print('\n lbd {} better than lbd {}: mse {} < {}.'.format(lbd, best_lbd, valid_err, best_err))
            best_err = valid_err
            best_W = win_W
            best_lbd = lbd
            best_model = copy.deepcopy(model)
    if best_model is None:
        return -1
    # Run the full GP prediction with the best parameters.
    z = target_dset.x; z = np.hstack((z, np.zeros_like(z)))
    noise_dict = {'output_index': z[:, 1:].astype(int)}
    prediction = np.exp(best_model.predict(z, Y_metadata = noise_dict))
    prediction[1] = 2*np.sqrt(prediction[1])
    prediction[0][np.isnan(prediction[0])] = None
    prediction[1][np.isnan(prediction[1])] = None
    testlen = target_dset('testy').size
    prediction[0][:-testlen]=None
    prediction[1][:-testlen]=None
        
    # Write the prediction to the db structure.
    db_data['padID'].append(str(target_dset)+' '+str(best_lbd)+' '+str(best_W))
    db_data['date'].append([revert_date(d) for d in target_dset.x])
    db_data['oil_pd'].append(np.exp(target_dset.y.flatten()))
    db_data['oil_pd_mean'].append(prediction[0].flatten())
    db_data['oil_pd_var'].append(prediction[1].flatten())
    helpers.write2db(TABLE_NAME, db_data, krzysz_frm=True)
    
db_data = defaultdict(list)
for well_id in range(150):
    compute_well_prediction(well_id, db_data=db_data, n_align=5)
