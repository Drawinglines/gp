# System modules
import sys
import datetime
from datetime import timedelta

# External modules
import MySQLdb
import numpy as np
from mrjob.job import MRJob, MRStep
from mrjob.protocol import PickleProtocol, RawProtocol, JSONProtocol

# Custom modules
import core_ml
import helpers

import trendFilterRobustCVX

DEBUG=1 
TABLE = 'bigprod_ts'
cnx = {'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com',
       'user': 'kris', 'passwd': 'datascience', 'db': 'public_data', 'port': 3306}
class MRExtractLines(MRJob):
    INTERNAL_PROTOCOL = PickleProtocol
    OUTPUT_PROTOCOL = JSONProtocol

    def configure_options(self):
        super(MRExtractLines, self).configure_options()
        self.add_passthrough_option(
            '--matchlen',
            type=int,
            help=("Length (in months) of the subsequence matching window.")
        )
        self.add_passthrough_option(
            '--nalign',
            type=int,
            help=("Number of timeseries to align with each well for coregionalization.")
        )
        self.add_passthrough_option(
            '--lbd',
            type=float,
            help=("Lambda parameters for Taha's line algorithm")
        )
        self.add_passthrough_option(
            '--test_time',
            type=int,
            help=("Num of years for testing")
        )
        self.add_passthrough_option(
            '--results_table',
            type=str,
            help=("Name of the results table to create and store results in.")
        )
        self.add_passthrough_option(
            '--lengthscale',
            type=int,
            help=("Lengthscale.")
        )
        self.add_passthrough_option(
            '--W',
            type=float,
            help=("W.")
        )
        self.add_passthrough_option(
            '--dealwithmissing',
            type=str,
            help=("Linear or zero-missing interpolation.")
        )

    def load_options(self, args):
        super(MRExtractLines, self).load_options(args)
        self.matchlen = self.options.matchlen or 7*12
        self.n_align = self.options.nalign or 1
        self.max_iters = 100
        self.test_time = self.options.test_time or helpers.convert_date('2008-01-01')
        self.results_table = self.options.results_table or 'test_table'
        self.lengthscale = self.options.lengthscale or 20
        self.W = self.options.W or 32.
        self.dealwithmissing = self.options.dealwithmissing or 'missing'

    def steps(self):
        return [
            MRStep(mapper=self.mapper_distribute_apis),      # Append to each test api all the training apis.
            MRStep(mapper_init=self.init_align_datasets,     # Connect to the db.
                   mapper=self.mapper_align_datasets,        # Download data from the db, format it, prepare alignment key-val pairs.
                   mapper_final=self.final_align_datasets,   # Close the db connection.
                   reducer=self.reducer_align_datasets),     # For each test/train dataset pair, align them as best as possible and compute the error.
            MRStep(reducer=self.reducer_find_closest),       # Match each test dset with its n_align nearest neighbors.
            MRStep(mapper_init=self.init_predict_gp,         # Connect to the db.
                   mapper=self.mapper_predict_gp,            # Download each dset and its n_align nearest neighbors, train a GP and run the prediction.
                   mapper_final=self.final_predict_gp)       # Close the db.
        ]
    
    def mapper_distribute_apis(self, _, all_apis):
        """ Attach to each api a list of all other apis. """
        for api in all_apis.split(','):
            # Only process the test apis, keep all the rest as training data.
            yield api, all_apis.split(',')
    
    def init_align_datasets(self):
        self.conn = MySQLdb.connect(**cnx)
        self.cursor = self.conn.cursor()

    def mapper_align_datasets(self, api, all_apis):
        # Download data from the db and format it properly.
        query = 'SELECT oil, proddays, dates, water FROM bigprod_ts WHERE API_prod = '+api[:-1]
        self.cursor.execute(query)
        oil_and_water_and_date = self.cursor.fetchall()
        well_data = helpers.process_query_results(oil_and_water_and_date, api, 
            self.test_time, self.dealwithmissing, test_well=(api[-1]=='s'))
        if well_data is not None:
            prodvals = np.atleast_2d(trendFilterRobustCVX.transform(well_data('trainX').flatten(),
                                                                    well_data('trainy').flatten()))
            for api2 in all_apis:
                # Only continue processing if exactly one of the apis is a test well.
                if int(api[:-1]) < int(api2[:-1]) and api[-1]=='s' and api2[-1]=='r':
                    yield (api[:-1], api2[:-1]), (prodvals.tolist(), 0)
                if int(api[:-1]) > int(api2[:-1]) and api2[-1]=='s' and api[-1]=='r':
                    yield (api2[:-1], api[:-1]), (0, prodvals.tolist())

    def final_align_datasets(self):
        # Close the db connection.
        self.cursor.close()
        self.conn.close()
            
    def reducer_align_datasets(self, apis, prodvals):  
        # Compute the distance matrix. 
        (api1, api2) = apis
        prodvals = list(prodvals)
        if len(prodvals) == 2: # If not, one of the apis is not an acceptable dataset.
            prodval1 = np.squeeze(prodvals[0][0] or prodvals[1][0])
            prodval2 = np.squeeze(prodvals[0][1] or prodvals[1][1])
            # Only align to test datasets.
            (dist1, align_pt1, y_shift1) = core_ml.\
                emr_match_subseq(prodval1, prodval2, subseq_len=self.matchlen)
            yield api1, (api2, dist1, align_pt1, y_shift1)

    def reducer_find_closest(self, api1, api2_and_dist_and_align_and_y_shifts):
        # Even with millions of wells, this list should not be larger than several megabytes.
        api2_and_dist_and_align_and_y_shifts = list(api2_and_dist_and_align_and_y_shifts)
        api2s = [item[0] for item in api2_and_dist_and_align_and_y_shifts]
        dists = [float(item[1]) for item in api2_and_dist_and_align_and_y_shifts]
        align_pts = [item[2] for item in api2_and_dist_and_align_and_y_shifts]
        y_shifts = [item[3] for item in api2_and_dist_and_align_and_y_shifts]

        best_ids = np.argsort(dists)[:self.n_align]
        yield api1, ([api2s[b_id] for b_id in best_ids],
                                 [align_pts[b_id] for b_id in best_ids],
                                 [y_shifts[b_id] for b_id in best_ids]) 

    def init_predict_gp(self):
        # Connect to the db.
        self.conn = MySQLdb.connect(**cnx)
        self.cursor = self.conn.cursor()

        # Connect to the results db.
        self.conn_res = MySQLdb.connect(host=cnx['host'],user=cnx['user'],passwd=cnx['passwd'],db='results')
        self.conn_res.autocommit(True)
        self.cursor_res = self.conn_res.cursor()

        # Create a new results table, if it's not already there.
        query = "SHOW TABLES LIKE '{}';".format(self.results_table)
        self.cursor_res.execute(query)
        tableexists = self.cursor_res.fetchone()
        if not tableexists:
            try:
                query = "CREATE TABLE {} (id varchar(32), date_ DATE, oil_pd DOUBLE, oil_pd_forecast_mean DOUBLE, oil_pd_forecast_var DOUBLE, best_aligned_oil_trn DOUBLE);".format(self.results_table)
                self.cursor_res.execute(query)
            except:
                pass

    def mapper_predict_gp(self, target_api, apis_and_alignpts_and_startids):
        # Given a target api, n_align closest apis and alignment data, 
        # run a coregionalized GP model to predict oil production on the target well.
        write_to_db = 1
        (align_ids, align_pts, align_starts) = apis_and_alignpts_and_startids
        # Download the target and auxiliary datasets from the db.
        query = 'SELECT oil, proddays, dates FROM bigprod_ts WHERE API_prod = '+target_api
        self.cursor.execute(query)
        oil_and_date = self.cursor.fetchall()
        target_ds = helpers.process_query_results(oil_and_date, target_api, 
                                                  self.test_time, dealwithmissing='zeros', test_well=True)
        aux_dss = []
        for aux_id, aux_api in enumerate(align_ids):
            query = 'SELECT oil, proddays, dates FROM bigprod_ts WHERE API_prod = '+aux_api
            self.cursor.execute(query)
            oil_and_date = self.cursor.fetchall()
            aux_ds = helpers.process_query_results(oil_and_date, aux_api, self.test_time, dealwithmissing='zeros')
            if aux_ds is not None:
                aux_dss.append(core_ml.align_sequences(target_ds, aux_ds, align_pts[aux_id], 
                                                       align_starts[aux_id], self.matchlen))
        try:
            # Remove zero-producing days as "missing data".
            target_ds.remove_missing_from_log()
            [aux_ds.remove_missing_from_log() for aux_ds in aux_dss]

            # Do the forecast.
            (model, win_W, valid_err) = core_ml.multi_gp([target_ds]+aux_dss, max_iters=self.max_iters, 
                fixed_hypers={'W':self.W, 'l':self.lengthscale, 'noise':None, 'kappa':None}, valid_period=None, n_cv_vals=None)
            z = target_ds.x; z = np.hstack((z, np.zeros_like(z)))
            noise_dict = {'output_index': z[:, 1:].astype(int)}
            prediction = np.asarray(model.predict(z, Y_metadata = noise_dict))
            prediction[1] = 2*np.sqrt(prediction[1])
        except np.linalg.linalg.LinAlgError:
            write_to_db = 0
        except ValueError:
            write_to_db = 0

        # Write the prediction to the db.
        if write_to_db:
            values = []
            best_match = aux_dss[0]
            all_xs = np.unique(np.hstack([target_ds.x.flatten(), best_match('trainX').flatten()]))
            for date_id, date in enumerate(all_xs):
                target_id = np.where(target_ds.x==date)[0]
                best_match_id = np.where(best_match('trainX')==date)[0]
                target_y = target_ds.y[target_id]
                pred_y = prediction[0][target_id]
                pred_var = prediction[1][target_id]
                bestmatch_y = best_match.y[best_match_id]
                values.append("('{}','{}',{},{},{},{})".format(
                    target_api, 
                    helpers.revert_date(int(date)), 
                    np.exp(float(target_y)) if target_y else np.nan,
                    np.exp(float(pred_y)) if pred_y else np.nan,
                    float(pred_var) if pred_var else np.nan,
                    np.exp(float(bestmatch_y)) if bestmatch_y else np.nan))
            query = "INSERT INTO {} VALUES {};".format(self.results_table, ','.join(values))
            query = query.replace('nan', 'NULL')
            query = query.replace('inf', 'NULL')
            self.cursor_res.execute(query)
            yield 1, 1
        else:
            yield 0, 0

    def final_predict_gp(self):
        # Close the db connections.
        self.cursor.close()
        self.conn.close()
        
        self.cursor_res.close()
        self.conn_res.close()

    def reducer_predict_gp(self, _, vals):
        yield 0, np.mean(list(vals))

if __name__=="__main__":
    MRExtractLines.run()
