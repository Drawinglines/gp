import MySQLdb
import os
import sys; sys.path.append('../')
import cPickle as pkl
import numpy as np
import helpers
import core_ml

#TABLE_NAME = 'joint_table_lbu'
IDS_TO_PROCESS_FNAME='emr_input_33'
TABLE_NAME = 'bigprod_ts'
TEST_STATE = 33 # DON'T USE OKLAHOMA!
MAX_TEST = 1000000
MATCH_LEN=7*12.
START_TRAIN='2000-12-31'
END_TRAIN='2007-12-31'
END_TEST='2014-12-31'
cnx = {'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com',
       'user': 'kris', 'passwd': 'datascience', 'db': 'public_data', 'port': 3306}
conn = MySQLdb.connect(**cnx)
cursor = conn.cursor()

print('Downloading training ids from the DB...')
train_ids_query = 'SELECT API_prod FROM '+TABLE_NAME+' WHERE state = '+str(TEST_STATE)+' LIMIT '+str(MAX_TEST)
cursor.execute(train_ids_query)
train_ids = [x[0] for x in cursor.fetchall()]
np.random.shuffle(train_ids)

print('Finding suitable test ids...')
# From among the train_ids, choose 1000 ids suitable for being test wells.
test_ids = []
for train_id_id, train_id in enumerate(train_ids):
    test_query = 'SELECT dates FROM '+TABLE_NAME+' WHERE API_prod = '+str(train_id)
    cursor.execute(test_query)
    dates = cursor.fetchall()[0][0].split(',')
    if helpers.convert_date(dates[0])<helpers.convert_date(START_TRAIN) and\
       END_TRAIN in dates and END_TEST in dates:
        test_query2 = 'SELECT oil FROM '+TABLE_NAME+' WHERE API_prod = '+str(train_id)
        cursor.execute(test_query2)
        oilprods = map(float, cursor.fetchall()[0][0].split(','))
        if oilprods[np.where(np.array(dates)==END_TRAIN)[0][0]] > 0:
            test_ids.append(train_id)
    print('{}/{}; {}/{}'.format(train_id_id, len(train_ids), len(test_ids), 1000))
    if len(test_ids) == 1000: 
        break

    
print('Downloaded {} apis from state {}. Found {} suitable test apis'.format(len(train_ids), TEST_STATE, len(test_ids)))

with open('test_ids_33', 'wt') as f:
      for test_id in test_ids:
          f.write(str(test_id)+'\n')

np.random.shuffle(train_ids)      
with open(IDS_TO_PROCESS_FNAME, 'wt') as f:
    for ds_id in train_ids[:-2]:
        if ds_id in test_ids:
            f.write(str(ds_id)+'s'+',') # This is a test well
        else:
            f.write(str(ds_id)+'r'+',') # This is a training well
    if train_ids[-1] in test_ids:
        f.write(str(train_ids[-1])+'s')
    else:
        f.write(str(train_ids[-1])+'r')
