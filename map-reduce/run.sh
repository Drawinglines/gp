RUNNER=${1:-local}
LENGTHSCALE=${2:-80}
W=${3:-32}
NALIGN=${4:-5}
Wunderscore=`echo $W | sed 's/\./_/g'`
RESULTS_TABLE="GP_l${LENGTHSCALE}_W${Wunderscore}_N${NALIGN}_TESTING_2"

echo "Outputting to table $RESULTS_TABLE"
cp ../helpers.py ../core_ml.py ../trendFilterRobustCVX.py src
cp ../helpers.py ../core_ml.py ../trendFilterRobustCVX.py ./
tar -C src -f drawing_gp_lines.tar.gz -z -c .

python EMR_full.py --conf-path=./cvxpy_install.conf --runner=$RUNNER -v --lengthscale=$LENGTHSCALE --W=$W --nalign=$NALIGN --results_table=$RESULTS_TABLE --bootstrap-action="s3://elasticmapreduce/bootstrap-actions/configure-hadoop -m mapred.task.timeout=3600000" --strict-protocols emr_input_short_33 > emr_result_33
