import numpy as np

import core_ml
import helpers

N_datasets=10
MATCH_LEN = 7*12

print('Preparing data...')
oil_ds = np.array([core_ml.Data(x=x, y=y, test_time=7., name='data') for x, y in helpers.load_data('data.p', min_active_months=MATCH_LEN*2, type='oil', normalized_monthly_vals=False, nanzero=True)][:N_datasets])
[oil_d.preprocess(standardize=False) for oil_d in oil_ds]
wor_ds = np.array([core_ml.Data(x=x, y=y, test_time=7., name='data') for x, y in helpers.load_data('data.p', min_active_months=MATCH_LEN*2, type='wor', normalized_monthly_vals=False, nanzero=True)][:N_datasets])
datasets = {'oil': oil_ds, 'wor': wor_ds}

print('Extracting the pq representation...')
pqs = core_ml.extract_pq(datasets, p_window=MATCH_LEN, stride=6, fname=None)

print('Running ffr...')
params = {'lam':0.3, 'SK': [50., 1000., 1000.], 'CK': [0.5, 0.5, 0.5]}
predictions = core_ml.ffr(pqs, params)

print('Printing predictions...')
helpers.print_predictions(oil_ds, predictions, pdf_fname='report_pqs.pdf', detrend=False)
