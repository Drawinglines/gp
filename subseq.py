import numpy as np
import cPickle as pk
from numpy.linalg import norm 

def getLength(oil):
    N = len(oil)
    lens = [len(x) for x in oil]
    for i in range(N):
        while oil[i][lens[i]-1] == 0:
            lens[i] -= 1
            if lens[i] == 0: break
    return lens

# The global parameters of the algorithm
mW = 50     # The matching window size
fW = 50     # The forecasting window size
nM = 30     # Number of subsequences used for matching
data = pk.load( open( "dataEasy.p", "rb" ) )    # Data
lens = getLength(data['oil'])       # Length of time series

def distance(tgt1, ts1, tgt2, ts2):
    ''' This function computes the distance between two subsequences.
    '''
    try:
        return norm(data['oil'][tgt1][ts1:ts1+mW] - data['oil'][tgt2][ts2:ts2+mW]) + \
            norm(data['days'][tgt1][ts1:ts1+mW] - data['days'][tgt2][ts2:ts2+mW]) + \
            norm(data['wor'][tgt1][ts1:ts1+mW] - data['wor'][tgt2][ts2:ts2+mW])
    except:
        import pdb; pdb.set_trace()
        return np.inf

def updateBuffer(buffer, iNew, jNew, dist):
    ''' Given an existing buffer, we want to insert the new observations
    into the buffer.
    '''
    if dist > buffer[nM-1,2]: return buffer
    ind = (buffer[nM,2] < dist).nonzero()[0].shape[0]
    buffer[ind+1:,:] = buffer[ind:-1,:]
    buffer[ind,:] = [iNew, jNew, dist]
    import pdb; pdb.set_trace()
    return buffer

def getBuffer(target, ts):
    ''' This function finds the top-nM similar subsequences.
    '''
    buffer = np.zeros((nM, 3))      # (i, ts, dist)
    objs = np.inf*np.ones((nM, 1))
    for i in range(0, len(data['oil'])):
        # Searching for the best match in ith time series
        jdist = np.inf    # The value of distance at the selected j
        jsel = 0        # The selected j
        if i == target: continue
        if lens[i] < (fW+mW): continue
        #
        for j in range(0, lens[i]-mW-fW):   # We are looking at the parts that can be useful
            dist = distance(target, ts, i, j)
            if dist < jdist:
                jsel = j
                jdist = dist
        # Now, update the buffer
        buffer = updateBuffer(buffer, i, jsel, jdist)
    return buffer

def subseq(target, ts):
    ''' This function gets the top-nM similar subsequences and averages them.
    We might want to implement the initial adjustment technique too.
    '''
    buffer = getBuffer(target, ts)
    seq = np.zeros((1,fW))
    for i in range(0, nM):
        seq += data['oil'][int(buffer[i,0])][int(buffer[i,1])+mW:int(buffer[i,1])+fW+mW]
    return seq/nM

# Main Function
if __name__ == "__main__":
    target = 2
    ts = 50
    #import pdb; pdb.set_trace()
    fore = subseq(target, ts)  # Get the forecast starting from t = 100
    actu = data['oil'][target][ts+mW:ts+fW+mW]
    print norm(fore-actu)/norm(actu)
