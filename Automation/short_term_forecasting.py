from __future__ import print_function
from builtins import range
from airflow.operators import PythonOperator
from airflow.models import DAG
from datetime import datetime, timedelta

import time
from pprint import pprint

seven_days_ago = datetime.combine(datetime.today() - timedelta(7),
                                  datetime.min.time())

args = { 'owner': 'airflow', 'start_date' : seven_days_ago }

# The forecasting DAG definition
dag = DAG(dag_id='short_term_forecasting', default_args=args)

def print_context(ds, **kwargs):
    pprint(kwargs)
    print(ds)
    return 'Whatever you return gets printed in the logs'

# Begin our actual function definitions
# Each preprocessing step accepts the main preprocessing S3 bucket
# as an argument, while each function and post processing step
# accepts the bucket specific to that method, as these differ for
# each method (ie, they shouldn't collide with each other)

def my_sleeping_function(random_base): time.sleep(random_base)

# Preprocessing function
def prep_function(): my_sleeping_function(30)

# SSM Functions
def ssm_pre(preprocessedS3): my_sleeping_function(18)
def ssm_function(ssmS3): my_sleeping_function(900)
def ssm_post(ssmS3): my_sleeping_function(8)

# GP Functions
def gp_pre(preprocessedS3): my_sleeping_function(15)
def gp_function(gpS3): my_sleeping_function(150)
def gp_post(gpS3): my_sleeping_function(11)

# FFR Functions
def ffr_pre(preprocessedS3): my_sleeping_function(15)
def ffr_function(ffrS3): my_sleeping_function(600)
def ffr_post(ffrS3): my_sleeping_function(9)

# Recurrent Neural Network functions
def rnn_pre(preprocessedS3): my_sleeping_function(20)
def rnn_function(rnnS3): my_sleeping_function(800)
def rnn_post(rnnS3): my_sleeping_function(7)

# Aggregation functions
# These need to know about all S3 buckets
def simple_ag_function(ssmS3, gpS3, ffrS3, rnnS3): my_sleeping_function(10)
def tree_ag_function(ssmS3, gpS3, ffrS3, rnnS3): my_sleeping_function(20)

# Organize the pipeline dependencies

# Ingest from MySQL to S3
mysql_ingest = PythonOperator(task_id='mysql_ingest', provide_context=True, python_callable=print_context, dag=dag)

# Preprocess in S3
prep_task = PythonOperator(task_id='preprocess', provide_context=True, python_callable=prep_function, dag=dag)
prep_task.set_upstream(mysql_ingest)

# We need to obtain the location in S3 where prepared data was written out
prepS3 = "S3 Bucket Details from Preprocessing Go Here"

# SSM Tasks
ssm_pre_task = PythonOperator(task_id='ssm_pre', python_callable=ssm_pre, op_kwargs={'preprocessedS3': prepS3}, dag=dag)
ssm_task = PythonOperator(task_id='ssm', python_callable=ssm_function, op_kwargs={'ssmS3': ""}, dag=dag)
ssm_post_task = PythonOperator(task_id='ssm_post', python_callable=ssm_post, op_kwargs={'ssmS3': ""}, dag=dag)

# SSM Dependencies
ssm_pre_task.set_upstream(prep_task);
ssm_task.set_upstream(ssm_pre_task);
ssm_post_task.set_upstream(ssm_task);


# GP Tasks
gp_pre_task = PythonOperator(task_id='gp_pre', python_callable=gp_pre, op_kwargs={'preprocessedS3': prepS3}, dag=dag)
gp_task = PythonOperator(task_id='gp', python_callable=gp_function, op_kwargs={'gpS3': ""}, dag=dag)
gp_post_task = PythonOperator(task_id='gp_post', python_callable=gp_post, op_kwargs={'gpS3': ""}, dag=dag)

# GP Dependencies
gp_pre_task.set_upstream(prep_task);
gp_task.set_upstream(gp_pre_task);
gp_post_task.set_upstream(gp_task);


# FFR Tasks
ffr_pre_task = PythonOperator(task_id='ffr_pre', python_callable=ffr_pre, op_kwargs={'preprocessedS3': prepS3}, dag=dag)
ffr_task = PythonOperator(task_id='ffr', python_callable=ffr_function, op_kwargs={'ffrS3': ""}, dag=dag)
ffr_post_task = PythonOperator(task_id='ffr_post', python_callable=ffr_post, op_kwargs={'ffrS3': ""}, dag=dag)

# FFR Dependencies
ffr_pre_task.set_upstream(prep_task);
ffr_task.set_upstream(ffr_pre_task);
ffr_post_task.set_upstream(ffr_task);


# RNN Tasks
rnn_pre_task = PythonOperator(task_id='rnn_pre', python_callable=rnn_pre, op_kwargs={'preprocessedS3': prepS3}, dag=dag)
rnn_task = PythonOperator(task_id='rnn', python_callable=rnn_function, op_kwargs={'rnnS3': ""}, dag=dag)
rnn_post_task = PythonOperator(task_id='rnn_post', python_callable=rnn_post, op_kwargs={'rnnS3': ""}, dag=dag)

# RNN Dependencies
rnn_pre_task.set_upstream(prep_task);
rnn_task.set_upstream(rnn_pre_task);
rnn_post_task.set_upstream(rnn_task);

simple_ag_task = PythonOperator(task_id='simple_aggregation', python_callable=simple_ag_function,
        						op_kwargs={'preprocessedS3': prepS3}, dag=dag)
# tree_ag_task = PythonOperator(task_id='tree_aggregation', python_callable=tree_ag_function,
#       						op_kwargs={'preprocessedS3': prepS3}, dag=dag)

simple_ag_task.set_upstream(ssm_post_task)
simple_ag_task.set_upstream(gp_post_task)
simple_ag_task.set_upstream(ffr_post_task)
simple_ag_task.set_upstream(rnn_post_task)

# Uncomment for tree aggregation
# tree_ag_task.set_upstream(ssm_task)
# tree_ag_task.set_upstream(gp_task)
# tree_ag_task.set_upstream(ffr_task)
# tree_ag_task.set_upstream(rnn_task)

mysql_store = PythonOperator(task_id='mysql_store', provide_context=True,
							python_callable=print_context, dag=dag)
mysql_store.set_upstream(simple_ag_task)

# mysql_store = PythonOperator(task_id='mysql_store_tree_ag', provide_context=True,
#							python_callable=print_context, dag=dag)
# mysql_store.set_upstream(tree_ag_task)