import MySQLdb

import sys
from collections import defaultdict
import datetime
import calendar

import numpy as np
import cPickle as pkl
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
try:
    from mpltools import color
    from mpltools import special
except:
    # Won't plot. That's fine for EMR.
    pass

import core_ml

def convert_date(absolute_date, year0=1900):
    """ Transform a yyyy-mm-dd date representation into an integer: the number of months since year0."""
    year = int(absolute_date[:4])
    month = int(absolute_date[5:7])
    return (year-year0)*12+month-1

def revert_date(time_from_year_0, year0=1900):
    """ Revert a converted date into the yyyy-mm-dd format. """
    month = np.mod(time_from_year_0, 12)+1
    year = int(time_from_year_0)/12
    return '{}-{:02d}-{:02d}'.format(year+year0, month, calendar.monthrange(year+year0, month)[1])

def process_query_results(oil_and_date, api, test_start, dealwithmissing, test_well=False):
    # Given a returned MySQL query response, extract normalized oil and date data.
    try:
        oil = np.asarray(map(float, oil_and_date[0][0].split(',')))
    except AttributeError:
        oil = np.asarray([])
    except:
        raise ValueError(api)
    oil[np.isnan(oil)] = 0.
    # Load production days 
    proddays = np.asarray(map(float, oil_and_date[0][1].split(',')))
    # Normalize oil by proddays unless proddays is zero.
    proddays[proddays==0.] = 1.
    oil = oil/proddays
    
    # Load dates and sort everything by date.
    dates = np.array([convert_date(d) for d in oil_and_date[0][2].split(',')])

    # Remove zeros, nans, infs.
    nonnan_ids = np.where(1-np.isnan(oil))[0]
    oil = oil[nonnan_ids]
    dates = dates[nonnan_ids]

    noninf_ids = np.where(1-np.isinf(oil))[0]
    oil = oil[noninf_ids]
    dates = dates[noninf_ids]

    nonzero_ids = np.where(oil != 0)[0]
    oil = oil[nonzero_ids]
    dates = dates[nonzero_ids]  

    (dates, oil) = _sum_unique_dates(dates, oil)
    date_ids = np.argsort(dates)
    dates = dates[date_ids]
    oil = oil[date_ids]

    # Remove outlier peaks.
    oil_tmp = oil
    oil_tmp[np.where(oil_tmp==0)[0]]=0.00001
    doil = np.exp(np.diff(np.log(oil_tmp))) # "Multiplicative derivative" of oil
    peakstarts = np.where(doil>5)[0] # Find peaks
    outlier_ids = []
    for peak in peakstarts:
        for peak_len in range(10):
            try:
                if doil[peak+peak_len] < 0.2:
                    outlier_ids.append((peak, peak+peak_len))
                    continue
            except:
                continue

    for (pk_start, pk_end) in outlier_ids:
        oil[pk_start+1:pk_end+1] = oil[pk_start]

    if dates.size>0 and test_well:
        well_data = core_ml.Data(x=dates, y=np.atleast_2d(oil).T, 
                                 test_time=test_start, name=str(api), 
                                 dealwithmissing=dealwithmissing)
        well_data.preprocess(standardize=False)
        return well_data

    elif dates.size>0:
        # A training well: make sure has enough data to be useful.
        well_data = core_ml.Data(x=dates, y=np.atleast_2d(oil).T, 
                                 test_time=test_start, name=str(api), 
                                 dealwithmissing=dealwithmissing, dates_str=dates_str)
        well_data.preprocess(standardize=False)
        if (well_data('trainX').size>0
            and well_data('trainX')[-1]-well_data('trainX')[0] >= 2*7*12):
            return well_data
    if test_well:
        raise Exception(oil_and_date)
    return None

def _sum_unique_dates(dates, oil):
    """ If dates contains duplicates, sum corresponding values in oil. Return unique dates and the corresponding oil sums. """
    dates_unique = np.unique(dates)
    oil_unique = np.zeros_like(dates_unique).astype(float)
    for d_id, d in enumerate(dates_unique):
        oil_unique[d_id] = oil[np.where(dates_unique==d)[0]].sum()
    return (dates_unique, oil_unique)

def parse_num(func, a):
    try:
        return func(a)
    except:
        print('Expected float, got {} instead'.format(type(a)))
        return np.nan

def readfromdb(min_active_months=30, normalized_monthly_vals=True, y_type='oil', year_0="1900-01-01", nanzero=False):
    """ Load data from a MySQL table. 

    Parameters:
    -----------
    table: currently can be one of
        - 'joint_table_lbu', the LBU data (for training)
        - 'lbu_forecasts_timeseries', LBU data forecasts (for training, with human predictions)
        - 'joint_table_cali', the full California dataset
    min_active_months: ignore wells that were inactive for too long.
    normalized_monthly_vals: divide production by days active each month.
    y_type: which kind of data to load. If a list, return a list of datasets, one per type. Can be one of: 'oil', 'water' (valid for any table), 'cumoil', 'cumwater' (only for LBU, training), 'co_actual', 'co_fore' (only in lbu_forecasts).
        - year_0: start counting months from here.
        - nanzero: if False, remove any  NaNs and Infs from the data (there will be missing months then). If True, fix them to zero.
    
    Returns:
    --------
    [(ts, {'type': ys})] - a list of times and dictionaries of values, with data types as keys.
    well_names - a list of ids, unique for each well.
    """
    type_ids = {'bigprod_ts': {'API_prod': 0,
                                        'state': 1,
                                        'dates': 2,
                                        'oil': 3,
                                        'water': 4,
                                        }}

    cnx = {'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com',
           'user': 'navjot', 'password': 'forgotit', 'db': 'public_data'}

    date_format = "%Y-%m-%d"
    t0 = datetime.strptime(year_0, date_format)

    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()
    print('Downloading the data from the DB...')
    query = 'SELECT * FROM navjot.bigprod_ts LIMIT 100'
    cursor.execute(query)
    row = cursor.fetchall()
    print('Done')
    well_names = []
    ts = []
    if type(y_type) != list:
        y_type = [y_type]
    ys = []
    
    for well_data_id, well_data in enumerate(row):
        sys.stdout.write('\r')
        sys.stdout.write('Reading wells from the database: {}%...'.format(int(100*float(well_data_id)/len(row))))
        sys.stdout.flush()
        well_names.append(well_data[0])
        t = np.array([np.ceil((datetime.strptime(date, date_format)-t0).days/30.4) for date in well_data[1].split(',')])
        t_ids = np.argsort(t)
        t = t[t_ids] # Need to sort everything by time in case the db is not.
        ts.append(t)
        ys.append(defaultdict(list))
        proddays = np.atleast_2d([parse_num(int, x) for x in well_data[type_id_per_table[table]['proddays']].split(',')]).T
        proddays = proddays[t_ids]
        for y_id, y_t in enumerate(y_type):
            ys[-1][y_t] = np.atleast_2d([parse_num(float,x) for x in well_data[type_id_per_table[table][y_t]].split(',')]).T[t_ids]
            if type(normalized_monthly_vals) == list and normalized_monthly_vals[y_id]:
                ys[-1][y_t] = ys[-1][y_t]/proddays
            elif type(normalized_monthly_vals != list) and normalized_monthly_vals:
                ys[-1][y_t] = ys[-1][y_t]/proddays
    # For each dataset/well, remove nan's (assumably catastrophic failures).
    # Additionally, remove the zero-production days, but keep the time indices untouched.
    for dataset_id in range(len(ts)):
        try:
            if nanzero:
                for y_t in y_type:
                    nan_ids = np.where(np.isnan(ys[dataset_id][y_t])==True)[0]
                    ys[dataset_id][y_t][nan_ids] = 0.

                    inf_ids = np.where(np.isinf(ys[dataset_id][y_t])==True)[0]
                    ys[dataset_id][y_t][inf_ids] = 0.      
                    
                    # Remove trailing zeros if there are any, for each type of data.
                    # In addition, remove the time ids for which all types are zero-tails.
                    zero_ids = np.where(np.cumsum(np.flipud(ys[dataset_id][y_t]))==0)[0]
                    first_empty_id = np.inf
                    if len(zero_ids) > 0:
                        first_empty_id = min(zero_ids[-1], first_empty_id)
                        ys[dataset_id][y_t] = ys[dataset_id][y_t][:-zero_ids[-1]]
                    
                    zero_ids = np.where(ys[dataset_id][y_t]==0)[0]
                    ys[dataset_id][y_t][zero_ids] = 1    # Because we will take log later.
                ts[dataset_id] = ts[dataset_id][:-first_empty_id if not np.isinf(first_empty_id) else 0]
            else:
                # Remove the datapoints corresponding to time indices at which at least one y-val is nan.
                nonnan_ids = np.where(np.all([np.isnan(ys[dataset_id][y_t])==False for y_t in y_type], axis=0))[0]
                ts[dataset_id] = ts[dataset_id][nonnan_ids]
                for y_t in y_type:
                    ys[dataset_id][y_t] = ys[dataset_id][y_t][nonnan_ids]

                nozero_ids = np.where(np.all(np.array([ys[dataset_id][y_t] for y_t in y_type]) !=0, axis=0))[0]
                ts[dataset_id] = ts[dataset_id][nozero_ids]
                for y_t in y_type:
                    ys[dataset_id][y_t] = ys[dataset_id][y_t][nozero_ids]

                noninf_ids = np.where(np.all([np.isinf(ys[dataset_id][y_t])==False for y_t in y_type], axis=0))[0]
                ts[dataset_id] = ts[dataset_id][noninf_ids]
                for y_t in y_type:
                    ys[dataset_id][y_t] = ys[dataset_id][y_t][noninf_ids]
                
        except TypeError:
            pass # Empty sequence; will be removed in next step.
    # Only keep sequences with at least one data type as long as required.
    longids = np.where([np.sum([y[y_t].shape[0] >= min_active_months for y_t in y_type]) for y in ys])[0]
    ys = [ys[y_id] for y_id in longids]
    ts = np.array(ts)[longids]
    return zip(ts, ys, well_names)

def print_steprep(datas, steprep, pdf_fname='report.pdf'):
    report_pdf = PdfPages(pdf_fname)
    dense_slopelengths = core_ml.slopelen_to_dense(steprep, cumsums=True)
    for data_id, data in enumerate(datas):
        fig = plt.figure()
        # Plot uptrended predictions.
        plt.subplot(2,1,1)
        #data.uptrend()
        plt.plot(data('trainX'), data('trainy'), 'k.')
        plt.plot(data('testX'), data('testy'), 'r.')
        break_locs = np.cumsum(steprep[data_id][1])
        plt.plot(data('trainX')[0]+break_locs, np.zeros_like(break_locs), 'r|', markersize=20)
        plt.xlim([data('trainX')[0], data('testX')[-1]])

        # Plot the line-representation.
        plt.subplot(2,1,2)
        #cur_x = data('trainX')[0]
        #for slope, length in zip(*steprep[data_id]):
        #    plt.plot([cur_x, cur_x+length], [slope, slope], 'k-')
        #    cur_x = cur_x+length
        plt.plot(dense_slopelengths[data_id], 'k-')
        #plt.xlim([data('trainX')[0], data('testX')[-1]])
        report_pdf.savefig(fig)
        plt.close(fig)
    report_pdf.close()

def _y_from_lines(lines, month, y0):
    y = y0
    slopelens = np.cumsum(lines[1])
    for m_id in range(month):
        slope_id = np.where(slopelens>month)[0]
        slope = lines[0][slope_id[0] if slope_id.size>0 else 0]
        y = y+slope
    return y

def print_lined_predictions(oil_ds, predictions, prediction_ids, align_ids, align_pts, align_starts, pdf_fname='report.pdf', cmap_name='winter', match_len=12*7, figsize=None, show_nn=True, dset_ids=None):
    """ Print each oil dataset, its line representation, and its closest-matching
    datasets defined by their ids and align-points. """
    #[oil_d.uptrend() for oil_d in oil_ds]
    if pdf_fname is not None:
        report_pdf = PdfPages(pdf_fname)
    cmap = plt.get_cmap(cmap_name)
    N_neighbors = len(align_ids[0])
    for data_id_id, data_id in enumerate(prediction_ids):
        data = oil_ds[data_id]
        match_id = data.get_month_id(-match_len)
        fig = plt.figure(figsize=figsize)
        # Plot target well.
        ax=plt.subplot(1,1,1)
        m, v = predictions[data_id_id]
        special.errorfill(data.x.flatten(), m.flatten(), 2*np.sqrt(v.flatten()), color='r', alpha_fill=0.1)
        plt.plot(data('trainX')[match_id], data('trainy')[match_id], 'go', markersize=10)
        # Plot matched-subseq wells.
        if show_nn:
            for neighbor_id in range(N_neighbors):
                neighbor = oil_ds[align_ids[data_id_id][neighbor_id]]
                aligned = core_ml.align_sequences(data, neighbor, align_pts[data_id_id][neighbor_id], 
                                                  align_starts[data_id_id][neighbor_id], match_len)
                plt.plot(aligned('trainX'), aligned('trainy'), '.', 
                         color=cmap(neighbor_id/float(N_neighbors)), lw=0, alpha=0.2)
        plt.plot(data('trainX'), data('trainy'), 'k.')
        plt.plot(data('testX'), data('testy'), 'r.')
        
        # Print the prediction.
        plt.plot(data.x.flatten(), m.flatten(), 'r-')
        plt.xlim([data.x[0], data.x[-1]])
        plt.ylim([min(m.min(), data.y.min()), min(20,max(m.max(), data.y.max()))])
        plt.xlabel('Months since 01/01/1900')
        plt.ylabel('Log oil per day')

        if pdf_fname is not None:
            report_pdf.savefig(fig)
            plt.close(fig)
    if pdf_fname is not None:
        report_pdf.close()
        
    if pdf_fname is None:
        return ax

def write2db(name, data, krzysz_frm=True):
    """ Tahas DB interface. Takes a "data" dictionary, for example:
    data = defaultdict(list)
    # First well
    data['padID'].append('well#1')
    data['date'].append(['1999-01-01','1999-02-01','1999-03-01'])
    data['oil'].append([12.0, 13.0, 14.0])
    data['water'].append([1.3, 1.5, 1.7]) ## ONLY IF withwater==True!
    data['oil_fore'].append([12.0, 13.0, 14.0])
    data['water_fore'].append([1.3, 1.5, 1.7]) ## ONLY IF withwater==True!
    # Second well
    data['padID'].append('well#2')
    data['date'].append(['1999-08-01','1999-09-01','1999-10-01'])
    data['oil'].append([120.0, 130.0, 140.0])
    data['water'].append([1.3, 1.5, 1.7])
    data['oil_fore'].append([12.0, 13.0, 14.0])
    data['water_fore'].append([1.3, 1.5, 1.7])
    # And so on.
    """

    # Connect to the results database
    cnx = { 'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com',
            'user':'navjot','password': 'forgotit','db': 'results'}
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor()

    # Drop table if exists
    query = "DROP TABLE IF EXISTS {};".format(name)
    cursor.execute(query)

    # Create the table
    if krzysz_frm:
        query = "CREATE TABLE {} (id varchar(32), date_ DATE, oil DOUBLE, oil_forecast_mean DOUBLE, oil_forecast_var DOUBLE);".format(name)
    else:
        query = "CREATE TABLE {} (id varchar(32), date_ DATE,oil DOUBLE,water DOUBLE,oil_forecast DOUBLE,water_forecast DOUBLE);".format(name)
    cursor.execute(query)

    # Insert the rows
    values = list()
    for (i, well) in enumerate(data['padID']):
        for j in range(len(data['oil_pd'][i])):
            if krzysz_frm:
                values.append( "('{}','{}',{},{},{})".format(well, data['date'][i][j], \
                    data['oil_pd'][i][j], data['oil_pd_mean'][i][j], data['oil_pd_var'][i][j]))
            else:
                values.append( "('{}','{}',{},{},{},{})".format(well, data['date'][i][j], \
                    data['oil'][i][j], data['water'][i][j], data['oil_fore'][i][j], data['water_fore'][i][j]))
    query = 'INSERT INTO {} VALUES {};'.format(name,','.join(values))
    query = query.replace('nan', 'NULL')
    query = query.replace('inf', 'NULL')
    cursor.execute(query)
    conn.commit()
    cursor.close()
    conn.close()
