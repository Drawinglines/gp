import numpy as np
import cvxpy as cvx 
import bayesopt as bo
import matplotlib.pyplot as plt  # Only for plotting


def modelAggregate(X, y):
	''' This function finds the weights for different algorithms based on their accuracy in predcition.
	Input:  y: nx1, where "n" is the size of the validation dataset.  
	-----   X: nxp, where "p" is the number of algorithms.
	-----   We assume that every column of X includes the prediction of 'y' by the corresponding algorithm.
	Output: w: px1, where 'w[i]' indicates the weight assigned to 
	'''
	w = cvx.Variable(X.shape[1])
	obj = cvx.Minimize( cvx.sum_squares(X*w - y) )
	constraints = [cvx.sum_entries(w) == 1, w >= 0]
	prob = cvx.Problem(obj, constraints)
	prob.solve()  # Set "verbose=True" for debugging
	return w.value, prob.value

def genSimpleData(n=100, p=3):
	# n: Size of the validation set
	# p: # Number of algorithms
	sig = np.random.uniform(0, 1, (p,1))			# STD of the noise in each expert
	y = np.random.standard_normal((n,1))
	X = np.dot( y, np.ones((1,p)) )
	for i in range(p):
		X[:,i] += sig[i]*np.random.standard_normal((n,))
	return X,y,sig

def genMixedData(n=1000, p=3,theta=0.5):
	''' This function is a simple data generator for generation of mixture of two regimes.
	'''
	sig1 = np.random.uniform(0, 1, (p,1))			# STD of the noise in the first regime
	sig2 = 1 - sig1
	# First group
	z1 = np.random.uniform(0, theta, (n,1))
	y1 = np.random.standard_normal((n,1))
	X1 = np.dot( y1, np.ones((1,p)) )
	for i in range(p):
		X1[:,i] += sig1[i]*np.random.standard_normal((n,))
	# Second group
	z2 = np.random.uniform(theta, 1, (n,1))
	y2 = np.random.standard_normal((n,1))
	X2 = np.dot( y1, np.ones((1,p)) )
	for i in range(p):
		X2[:,i] += sig2[i]*np.random.standard_normal((n,))
	# Concatenation
	return np.vstack((z1,z2)), np.vstack((X1,X2)), np.vstack((y1,y2))

# Experiment with simple data
X,y,sig = genSimpleData()
w,_ = modelAggregate(X, y)
# Note that the lower the noise, the higher the weight received in aggregation
print "Simple model aggregation: the lower the noise, the higher the weight received in aggregation."
#import pdb; pdb.set_trace()
for i in range(sig.shape[0]):
	print "Expert #{} with std={} received weight w={}".format(i, sig[i,0], w[i,0])

# Experiments with decision tree aggregation
theta = 0.35	# This is the threshold of different regimes
z,X,y = genMixedData(theta=theta)

# This function splits the data on variable z with threshols theta and fits two 
# aggregations.  The reports the performance of 
def evaluateSample(theta):
	_,val1 = modelAggregate(X[z[:,0]<=theta, :], y[z[:,0]<=theta,:])
	_,val2 = modelAggregate(X[z[:,0]>theta, :], y[z[:,0]>theta,:])
	return val1+val2

# Illustrating the behavior of the function
# WARNING: This part may take a long time.
t = np.arange(np.min(z)+1e-4, np.max(z)-1e-4, 0.01)
s = np.array( [evaluateSample(tt) for tt in t] )
plt.plot(t, s, linewidth=2.0)
plt.xlabel('theta')
plt.ylabel('Objective function')
plt.title('Loss as a function of splitting point theta.')
plt.grid(True)
plt.show()

# The algorithm for greedy splitting: this is going to be used in each iteration of decision tree building
params = {} 
params['n_iterations'] = 100
params['n_init_samples'] = 10
params['crit_name'] = "cSum(cEI,cDistance)"
params['crit_params'] = [1, 0.5]
params['kernel_name'] = "kMaternISO3"
params['verbose_level'] = 0  # Keep quiet, show only errors and warnings
n = 1                     # n dimensions
lb = np.ones((n,))*(np.min(z)+1e-4)
ub = np.ones((n,))*(np.max(z)-1e-4)
mvalue, x_out, error = bo.optimize(evaluateSample, n, lb, ub, params)
print "\nExperiments with decision tree model aggregation."
print "Accuracy: truth={},\t bo_result={}".format(theta,x_out[0])

