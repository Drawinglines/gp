#!/usr/bin/python
from numpy.linalg import norm, solve
import matplotlib.pyplot as plt
import cvxpy as cvx
import numpy as np
import csv

def lambdaMax(y):
    """ This function find the smallest lambda that makes the piecewise approximation
    a single line.
    """
    T = y.shape[0]
    I = np.eye(T-2)
    O = np.zeros((T-2,1))
    D = np.concatenate([I,O,O], axis=1)\
        + np.concatenate([O,-2*I,O], axis=1)\
        + np.concatenate([O,O,I], axis=1)
    return norm(solve(np.dot(D,D.transpose()), D*y), np.inf)
    
def tsSegment(x, D):
    """ This function computes the indexes of x in which a change point has happened.
    """
    z = np.abs(np.dot(D, x))        # Taking the second order difference of x
    th = 0.1
    z[ z < th*np.mean(z) ] = 0      # Removing the very small changes
    knots = np.nonzero(z)[0] + 1 # KRZ
    
    return knots

def step_representation(x, y):
    x = x.flatten()
    y = y.flatten()
    criticals = np.concatenate([[0], np.where(np.diff(y, n=2)>1e-6)[0], [y.size-1]])
    lengths = np.diff(x[criticals])
    slopes = np.diff(y[criticals])/lengths
    return (slopes, lengths)

def robustL1trendFilter(times, y, lmbd):
    ''' This function find a piecewise linear trend for the input.
    Inputs: y: the original (N,) time series;  lmbd: the regularization parameter.
    Outputs: x: the trend;  segs: the change points in the time series.
    '''
    times = times.flatten()
    y = y.flatten()
    lam = lambdaMax(y)*lmbd
    T = y.shape[0]
    # Create a tridiagonal matrix
    D = np.concatenate( [np.diag(np.ones((T-2,))), np.zeros((T-2,2))], axis=1) \
        + np.concatenate( [np.zeros((T-2,1)), np.diag(-2*np.ones((T-2,))), np.zeros((T-2,1))], axis=1) \
        + np.concatenate( [np.zeros((T-2,2)), np.diag(np.ones((T-2,)))], axis=1)
    Z = np.diag(1.0*(y!=0))

    lines = cvx.Variable(T)
    objective = cvx.Minimize(cvx.norm(Z*lines - y, 1) + lam*cvx.norm(D*lines,1))
    prob = cvx.Problem(objective)
    try:
        prob.solve(solver=cvx.CVXOPT)
    except:
        return ([0], [times[-1]-times[0]])
    # Now segment the time series
    # segs = tsSegment(x.value, D)
    slopes, lengths = step_representation(times, np.array(lines.value))
    return slopes, lengths

def transform(times, y, lmbd=0.01):
    slopes, lengths = robustL1trendFilter(times, y, lmbd/(y.max()-y.min()+1e-6))
    y_dense = []
    for (slope, length) in zip(slopes, lengths):
        y_dense.extend((np.ones(length)*slope).tolist())
    y_dense = np.cumsum(y_dense)
    return y[0]+y_dense

## Main Function
if __name__=="__main__":
    oil = np.random.standard_normal((120,))
    x =  robustL1trendFilter(np.arange(120), oil, 0.01)[0]
    print(x)










