import sys
import copy 
import time
from multiprocessing import Pool
from collections import defaultdict

import matplotlib
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import scipy.io as sio
from scipy.spatial.distance import cdist
import sklearn.linear_model
import GPy
import numpy as np 

# Added for clustering
from scipy.sparse.linalg import eigs
from numpy import linalg as la
from numpy.linalg import norm, solve

# 'copy' needs to be imported here b/c GPy stupidly 
# imports numpy * which overwrites the standard 'copy' module!
import copy

import helpers
import trendFilterRobustCVX
from trendFilterRobustCVX import robustL1trendFilter

class LinearTrend(object):
    def __init__(self):
        self.linearizer = sklearn.linear_model.LinearRegression(fit_intercept=True, normalize=False)
        self._detrended = False

    def detrend(self, data, x_pred=None, y_pred=None):
        """ Subtract a linear trend from y."""
        if data._detrended is False:
            self.linearizer.fit(data('trainX'), data('trainy'))
        if y_pred is None and data._detrended is False:
            data.y = data.y-self.linearizer.predict(data.x)
            data._detrended = True
        elif y_pred is not None:
            y_pred = y_pred-self.linearizer.predict(x_pred)
            return y_pred

    def uptrend(self, data, x_pred=None, y_pred=None):
        """ Add the linear trend to y. """
        if data._detrended is False:
            if y_pred is not None:
                return y_pred
            else:
                return
        if y_pred is None:
            data.y = data.y+self.linearizer.predict(data.x)
            data._detrended = False
        else:
            y_pred = y_pred+self.linearizer.predict(x_pred)
            return y_pred

class Data(object):
    def __init__(self, y=None, x=None, name='dataset.mat', test_time=0, dealwithmissing='zeros', dates_str=None):
        """ This object holds training and test data and contains methods 
        to normalize and de-trend the data, as well as methods to revert 
        these operations. 
        -----------

        Parameters:
        y - full data (training and validation/test) outputs
        x - full data input points
        name - if y and x are not given, this is a name of a matlab dataset file.
        test_time - time for testing, in years.
        dealwithmissing - if 'zeros', then missing months receive "0" for their y-value. If "linear", we fill missing months with a linear interpolation between the beginning and end of the missing period.
        """
        assert y.shape[0] == x.shape[0], "Number of input and output samples does not match!"
        self.name = name
        self.normalized = False
        self.test_time = test_time
        
        if dealwithmissing == 'missing':
            self.x = x
            self.y = y
            self.dates_str = dates_str
        else:
            # Fill the missing values (if any) either with zeros or using linear interpolation.
            x_periods = np.diff(x.flatten())
            y_slopes = np.diff(y.flatten())/x_periods
            self.N = x_periods.sum()
            self.x = -np.ones((self.N, 1))
            self.y = -np.ones((self.N, 1))
            cur_id = 0
            remove_mask = []
            for x_id, x_period in enumerate(x_periods):
                self.x[cur_id] = x[x_id]
                self.y[cur_id] = y[x_id]
                cur_id = cur_id+1
                if x_period > 1:
                    for x_missing_id in range(1, int(x_period)):
                        self.x[cur_id] = x[x_id]+x_missing_id
                        if dealwithmissing=='zeros':
                            self.y[cur_id][0] = 0.00001
                        elif dealwithmissing=='linear':
                            self.y[cur_id][0] = y[x_id]+y_slopes[x_id]*x_missing_id
                        cur_id = cur_id+1

        self._xrange = float(self.x.max()-self.x.min())
        self._xmin = self.x.min() 
        self._range = self.y.max()-self.y.min()
        self._mean = self.y.mean()
        self._trend = LinearTrend()
        self._detrended = False

    def closest_existing_date_id(self, time_shift):
        if time_shift >= 0:
            start_date = self.x[0]
        else:
            start_date = self.x[-1]
        desired_date = start_date+time_shift
        closest_date_id = np.where(self.x>=desired_date)[0][0]
        return closest_date_id

    def __call__(self, which_set='trainX'):
        """ Returns training/test inputs/outputs, depending on the call value. Can be called with
        'trainX', 'trainy', 'testX', 'testy'. """
        test_start_id = np.where(self.x>=self.test_time)[0]
        if test_start_id.size > 0:
            test_start_id = test_start_id[0]
        else:
            test_start_id = self.x.size
        if which_set=='trainX':
            return self.x[:test_start_id]
        elif which_set=='testX':
            return self.x[test_start_id:]
        elif which_set=='trainy':
            return self.y[:test_start_id]
        elif which_set=='testy':
            return self.y[test_start_id:]
        elif which_set=='trainDatesStr':
            return self.dates_str[:test_start_id]
        else:
            raise ValueError('Please choose one of [trainX, testX, trainy, testy]')

    def detrend(self, y_pred=None):
        return self._trend.detrend(self, self.x, y_pred)
            
    def uptrend(self, y_pred=None):
        return self._trend.uptrend(self, self.x, y_pred)
        
    def remove_missing_from_log(self):
        """ Remove all the zero-valued entries as "missing". This assumes the data is in log-space."""
        zero_ids = np.where(self.y < -5)[0]
        mask = np.ones_like(self.y).astype(bool)
        mask[zero_ids] = False
        self.x = np.atleast_2d(self.x[mask]).T
        self.y = np.atleast_2d(self.y[mask]).T

    def preprocess(self, y_pred=None, standardize=True, takelog=True):
        """ If y_pred==None, take log of y, normalize y to zero mean and norm 1. Otherwise, preprocess y_pred. """
        if takelog:
            self.y = np.log(self.y+1e-5)
        if standardize and y_pred is None and not self.normalized:
            self._mean = self('trainy').mean()
            self._range = self('trainy').max() - self('trainy').min()
            if standardize:
                self.y = (self.y-self._mean)/self._range
            self.normalized = True
        elif standardize:
            return (y_pred-self._mean)/self._range
        
    def postprocess(self, y_pred=None):
        """ If y_pred==None, de-normalize self.y. Otherwise, de-normalize y_pred. """
        if y_pred is None:
            self.y = (self.y+self._mean/self._range)*self._range
            self.y = np.exp(self.y)
            self.normalized = False
        else:
            return np.exp((y_pred+self._mean/self._range)*self._range)

    def __str__(self):
        return self.name

def multi_gp(datas, optimizer=None, max_iters=1000., fixed_hypers={'noise': 0.01, 'W': 1., 'kappa': 0.01, 'l': 15.}, valid_period=None, n_cv_vals=10):
    """ Train a coregionalized GP on multiple datasets.
        Parameters:
        -----------
        datas - a list of Data objects: first elem is the target data, others are auxiliary datas
        optimizer - which GPy optimizer to use (see GPy docs)
        max_iters - max iters of GP hyperparam training
        fixed_hypers - list of hyperparameters to use with GPy
        valid_period - if not None, should be an integer indicating how many months' data to cut off from
        
        valid_period - cut off this many months from the head of trainX/trainy for validation of the coregionalization coefficient W
        n_cv_vals - run cross-validation on this many vals of the coeff, in linspace btwn 0. and 1.
        
        Returns:
        --------
        model - trained GP 
    """

    # Initialize the model to reasonable values.
    K = GPy.kern.RBF(input_dim=1, variance=1., lengthscale = 15. if fixed_hypers is None else fixed_hypers['l'])
    icm = GPy.util.multioutput.ICM(input_dim=1, 
                                   num_outputs=len(datas), 
                                   kernel=K,
                                   W_rank=len(datas))
    if valid_period is None:
        model = GPy.models.GPCoregionalizedRegression([d('trainX') for d in datas], 
                                                      [d('trainy') for d in datas], 
                                                      kernel=icm)
        y_range = datas[0]('trainy').max() - datas[0]('trainy').min()
        model.mixed_noise = fixed_hypers['noise'] or 0.01*y_range
        model.ICM.B.W = fixed_hypers['W'] or 1. # High W => More similar to neighbors.
        model.ICM.B.kappa = fixed_hypers['kappa'] or 0.01*y_range # High kappa => Less smoothing.

        # Run prediction on the validation data.
        best_mse = np.nan
        best_W = fixed_hypers['W']
    else:
        best_mse = np.inf
        best_W = -np.inf
        for W_id, W in enumerate(np.linspace(0.01, 30, n_cv_vals)):
            model = GPy.models.GPCoregionalizedRegression([d('trainX')[:-valid_period] for d in datas], 
                                                          [d('trainy')[:-valid_period] for d in datas], 
                                                          kernel=icm)
            y_range = datas[0]('trainy')[:-valid_period].max() - datas[0]('trainy')[:-valid_period].min()
            model.mixed_noise = fixed_hypers['noise'] or 0.01*y_range
            model.ICM.B.W = W
            model.ICM.B.kappa = fixed_hypers['kappa'] or 0.01*y_range # High kappa => Less smoothing.
            # Run prediction on the validation data.
            z = datas[0]('trainX')[valid_period:]
            y_valid = datas[0]('trainy')[valid_period:]
            z = np.hstack((z, np.zeros_like(z)))
            noise_dict = {'output_index': z[:,1:].astype(int)}
            y_pred = model.predict(z, Y_metadata=noise_dict)
            # Compute the mse and save best result so far.
            mse = np.mean(np.abs(y_valid-y_pred[0]))
            if mse < best_mse:
                best_mse = mse
                best_W = W
    return (model, best_W, best_mse)

def slopelen_to_dense(slopelengths, cumsums=False):
    # Convert a slopes-lengths representation into a dense point representation.
    dsets = []
    for dset_n in range(len(slopelengths)):
        dsets.append([])
        for (slope, length) in zip(*slopelengths[dset_n]):
            dsets[-1] = dsets[-1] + list(np.ones(length)*slope)
        dsets[-1] = np.cumsum(dsets[-1]) if cumsums else np.array(dsets[-1])
    return np.array(dsets)

def match_subseqs(slopelengths, subseq_len=7*12, subseq_stride=1, N_best=1, shortest_future=4*12):
    """ For each slope/length serues representation in slopelengths, extract
    all (w.r.t. the given subseq_stride) subsequences of length subseq_len
    from every other slopelength series. Then, find N_best such subsequences which
    align best to the end of the original sequence. 
    
    Return:
    align_ids - a list of numpy arrays; if align_ids[i][j] = k, then the kth
        dataset is the jth best aligned to the ith dataset.
    align_pts - align_pts[i][j] = t if dset[align_ids[i][j]] needs to be shifted by t 
        months to align best with the ith dataset *assuming the datasets start at the 
        same absolute month* (in practice, shift by the absolute difference too).
    align_start - align_start[i][j] = t if the jth best-aligned dataset to i has its
        best alignment starting t months after its beginning time.
    shortest_future - min length of the matched time series after the matching segment.
"""
    N_data = len(slopelengths)
    # Convert the slope/length representation into dense timeseries. 
    # TODO: This should probably be done by the timeseries extractor,
    # to save up on computation time.
    dsets = slopelen_to_dense(slopelengths)
    
    # For each series, extract the final subsequence and find N_best best-matching
    # windows among all the other subsequences.
    align_ids, align_pts, align_start, errs = ([], [], [], [])
    for dset_id, dset in enumerate(dsets):
        sys.stdout.write('\r')
        sys.stdout.write('{}%'.format(int(float(dset_id)/len(dsets))))
        sys.stdout.flush()
        match_me = dset[-subseq_len:]
        local_subseq_len = min(match_me.shape[0], subseq_len)
        match_start = len(dset)-local_subseq_len
        align_ids.append(np.array([]).astype(int))
        align_start.append(np.array([]))
        align_pts.append(np.array([]))
        errs.append(np.array([]))
        for other_dset_id in range(0, dset_id)+range(dset_id+1, N_data):
            matching_dset = dsets[other_dset_id]
            # Skip datasets shorter than the required subseq length.
            if len(matching_dset) < (local_subseq_len+shortest_future):
                continue
            best_err = np.inf
            best_start = -1
            for window_start in range(0, len(matching_dset)-local_subseq_len-shortest_future, subseq_stride):
                im_matching = matching_dset[window_start:window_start+local_subseq_len]
                err = subseq_d(match_me, im_matching) # Current window's error.
                if err < best_err:
                    best_err = err # Best window for the current dataset.
                    best_start = window_start # Best starting point for the current dataset.
            # Check if the current dataset is good enough to join the best bunch.
            error_position = np.searchsorted(errs[-1], best_err)
            if align_ids[-1].size < N_best:
                # If there's still space, insert the new id/starting point into the saved array.
                align_ids[-1] = np.insert(align_ids[-1], error_position, other_dset_id)
                align_pts[-1] = np.insert(align_pts[-1], error_position, match_start-best_start)
                align_start[-1] = np.insert(align_start[-1], error_position, best_start)
                errs[-1] = np.insert(errs[-1], error_position, best_err)
            elif error_position < N_best:
                align_ids[-1][error_position] = other_dset_id
                align_pts[-1][error_position] = match_start-best_start
                align_start[-1][error_position] = best_start
                errs[-1][error_position] = best_err
    print('Done.')
    return (align_ids, align_pts, align_start, errs)


def emr_match_subseq(prodval1, prodval2, subseq_len=7*12, shortest_future=7*12):
    # if coords != 'all':
    #     prodval1 = prodval1[coords,:]
    #     prodval2 = prodval2[coords,:]
    prodval1 = np.atleast_2d(prodval1)
    prodval2 = np.atleast_2d(prodval2)
    match_me = prodval1[:,-subseq_len:]
    local_subseq_len = match_me.shape[1]
    match_start = prodval1.shape[1]-local_subseq_len # 116-84=32

    best_err = np.inf
    best_start = -1

    for window_start in range(0, max(0, prodval2.shape[1]-local_subseq_len-shortest_future)):
        err = subseq_d(match_me, prodval2[:,window_start:window_start+local_subseq_len])
        if err < best_err:
            best_err = err
            best_start = window_start
    return (best_err, match_start-best_start, 0)

def align_sequences(target_ds, aux_ds, align_pt, align_start, match_len):
    """ Align sequence aux_ds with sequence target_ds:
    align_pt - number of months to shift aux_ds in time necessary
        to align the two sequences.
    align_start - month in aux_ds where the aligned part starts.
    match_len - length of the aligned segment.
    """
    shifted_aux = copy.deepcopy(aux_ds)

    x_shift = target_ds('trainX')[0]-aux_ds('trainX')[0]

    y_shift = align_start

    shifted_aux.y = shifted_aux.y+y_shift
    shifted_aux.x = shifted_aux.x+x_shift+align_pt
    shifted_aux.test_time = shifted_aux.test_time+x_shift+align_pt
    return shifted_aux
    
def subseq_d(s1, s2):
    # Subsequence distance function. Normalize each dimension by the norm
    # of the target's respective dimension first, then compute the sum
    # of squared distances of all dimensions.
    #normalizer = np.array([[np.sqrt(np.sum(s1[ax_id])**2)] for ax_id in range(s1.shape[0])])
    return np.sqrt(np.sum((s1-s2)**2, axis=1)).sum()

def extract_pq(datas, p_window=84, stride=6, fname=None):
    if fname is not None:
        try:
            print("Reading PQ data from {}!".format(fname))
            return pkl.load(open(fname, 'rb'))
        except IOError:
            print('{} does not exist!'.format(fname))
    
    q_window = datas['oil'][0].N-datas['oil'][0].test_start # WARNING: this assumes no missing data! (1 month = 1 datapoint)
    P_train = defaultdict(list)
    Q_train = defaultdict(list)
    P_test = defaultdict(list)
    for y_oil in [oil_d('trainy') for oil_d in datas['oil']]:
        for window_start in range(0, y_oil.shape[0]-stride-2*p_window, stride):
            P_train['oil'].append(y_oil[window_start:window_start+p_window])
            Q_train['oil'].append(y_oil[window_start+p_window:window_start+2*p_window])
        P_test['oil'].append(y_oil[-p_window:])

    for y_oil in [oil_d('trainy') for oil_d in datas['wor']]:
        for window_start in range(0, y_oil.shape[0]-stride-2*p_window, stride):
            P_train['wor'].append(y_oil[window_start:window_start+p_window])
            Q_train['wor'].append(y_oil[window_start+p_window:window_start+2*p_window])
        P_test['wor'].append(y_oil[-p_window:])
    # DETREND: for each (p, q), subtract mean(p) from p, q.
    P_train['oil'] = np.array(P_train['oil']).squeeze()
    P_train['wor'] = np.array(P_train['wor']).squeeze()
    P_test['oil'] = np.array(P_test['oil']).squeeze()
    P_test['wor'] = np.array(P_test['wor']).squeeze()
    Q_train['oil'] = np.array(Q_train['oil']).squeeze()
    Q_train['wor'] = np.array(Q_train['wor']).squeeze()
    result = (P_train, Q_train, P_test)
    if fname is not None:
        pkl.dump(result, open(fname, 'wb'))
    return result

def kernel(data1, data2, CK, SK):
    # The distance metric parameters
    cO, cW, cD, sO, sW, sD = (CK[0], CK[1], CK[2], SK[0], SK[1], SK[2])
    KO = cdist(data1['oil'], data2['oil']);   # Compute the kernel and normalize it
    KW = cdist(data1['wor'], data2['wor']);   # Compute the kernel and normalize it
    return cO/(sO + KO) + cW/(sW + KW)
    
def ffr(datas, params):
    """ Function to function regression using GPs.
    Parameters:
    datas -- a tuple with three elements (P_train, Q_train, P_test). 
    Each element is a dictionary with matrix keys.
    params -- lam,CK,SK

    Returns:
    m -- mean prediction for Q_test (paired with P_test)
    v -- predictive variance for Q_test
    """
    print('FFRing')
    debias = 3;     # Number of last data point to use for detrending
    (P_train, Q_train, P_test) = datas            # Unpack data
    lam,CK,SK = (params['lam'],params['CK'],params['SK'])     # Unpack the parameters
    
    # Remove the mean
    biasTr = np.mean(P_train['oil'], axis=1, keepdims=True)
    P_train['oil'] = P_train['oil'] - biasTr
    Q_train['oil'] = Q_train['oil'] - biasTr
    biasTs = np.mean(P_test['oil'], axis=1, keepdims=True)
    
    # Computing the kernels
    K = kernel(P_train, P_train, CK, SK)
    k = kernel(P_train, P_test, CK, SK)
    Ktest = kernel(P_test, P_test, CK, SK)
    Kinvk = solve(K+lam*np.eye(K.shape[0]), k)
    
    #
    # Compute the output
    m = np.dot(Kinvk.T, Q_train['oil']-P_train['oil'][:,-debias:].mean(axis=1,keepdims=True))
    # Ensuring the continuity of the forecasts at the beginning of the interval
    m = m + P_test['oil'][:,-debias:].mean(axis=1,keepdims=True)
    v = np.diag( Ktest + lam*np.eye(Ktest.shape[0]) - np.dot(k.T, Kinvk) )
    return zip(m.tolist(), v.tolist())
